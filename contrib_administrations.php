<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Installation/maj du plugin.
 *
 * Crée les champs categorie et préfixe pour les rubriques
 *
 * @param string $nom_meta_base_version Nom de la meta dans laquelle sera rangée la version du schéma
 * @param string $version_cible         Version du schéma de données en fin d'upgrade
 *
 * @return void
 */
function contrib_upgrade(string $nom_meta_base_version, string $version_cible) : void {
	$maj = [];

	// Ajout des colonnes supplémentaires dans les tables spip_rubriques et spip_articles.
	include_spip('inc/cextras');
	include_spip('base/contrib_declarations');
	cextras_api_upgrade(contrib_declarer_champs_extras(), $maj['create']);

	// Ajout de la tables des demandes d'inscription
	$maj['2'] = [
		['maj_tables', ['spip_inscriptions']]
	];

	// Ajout de la tables des demandes d'inscription
	$maj['3'] = [
		['sql_alter', 'TABLE spip_inscriptions ADD topic_id bigint(21) DEFAULT 0 NOT NULL AFTER parametres']
	];

	// Ajout de la tables des demandes d'inscription
	$maj['4'] = [
		['sql_alter', 'TABLE spip_inscriptions ADD descriptif tinytext DEFAULT "" NOT NULL AFTER email']
	];

	// Ajout de la tables des demandes d'inscription
	$maj['5'] = [
		['sql_drop_table', 'spip_inscriptions']
	];

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Désinstalle les données du plugin.
 *
 * Supprime les champs categorie et préfixe pour les rubriques
 *
 * @param string $nom_meta_base_version Nom de la meta dans laquelle sera rangée la version du schéma
 *
 * @return void
 */
function contrib_vider_tables(string $nom_meta_base_version) : void {
	// Suppression des colonnes ajoutées à l'installation : on ne le fait pas car ce plugin va être fusionné
	// et donc il est nécessaire de conserver ces colonnes

	// on efface la meta du schéma du plugin
	effacer_meta('contrib');

	// on efface la meta du schéma du plugin
	effacer_meta($nom_meta_base_version);
}
