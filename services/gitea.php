<?php
/**
 * Ce fichier contient l'ensemble des constantes et des utilitaires nécessaires au fonctionnement du service web Gitea.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_GITEA_SPIP_ENDPOINT_BASE_URL')) {
	/**
	 * Préfixe des URL du service web de Gitea SPIP.
	 */
	define('_GITEA_SPIP_ENDPOINT_BASE_URL', 'https://git.spip.net/api/v1/');
}

// ------ UTILITAIRES ------

/**
 * Construit l'URL de la requête Gitea correspondant à la demande utilisateur.
 *
 * @internal
 *
 * @param array      $parametres_path  Liste des paramètres de path à accoler dans l'ordre fourni.
 * @param null|array $parametres_query Liste des paramètres de query à ajouter.
 *
 * @return string L'URL de la requête au service
 */
function gitea_construire_url(array $parametres_path, ?array $parametres_query = []) : string {
	// Récupération du token d'autorisation d'accès à l'API
	include_spip('inc/config');
	$token = lire_config('contrib/token');

	// Construire l'URL de l'api sollicitée
	// -- la partie path
	$url = _GITEA_SPIP_ENDPOINT_BASE_URL
		   . implode('/', $parametres_path)
		   . "?access_token={$token}";

	// -- la partie query éventuelle qui se rajoute au token
	if ($parametres_query) {
		$query = '';
		foreach ($parametres_query as $_champ => $_valeur) {
			$query .= '&'
				. $_champ
				. '='
				. rawurlencode($_valeur);
		}
		$url .= $query;
	}

	return $url;
}

/**
 * Renvoie, à partir de l'url du service, le tableau des données demandées.
 * Le service utilise dans ce cas une chaine JSON qui est décodée pour fournir
 * le tableau de sortie. Le flux retourné par le service est systématiquement
 * transcodé dans le charset du site avant d'être décodé.
 *
 * @uses recuperer_url()
 *
 * @param string     $url     URL complète de la requête au service web concerné.
 * @param null|array $options Options du recuperer_url.
 *
 * @return array Tableau de la réponse à la requête
 */
function gitea_requeter(string $url, ?array $options = []) : array {
	// Initialisation de la réponse
	$reponse = [];

	// Mise au point des options
	include_spip('inc/distant');
	if (!isset($options['transcoder'])) {
		$options['transcoder'] = true;
	}
	$flux = recuperer_url($url, $options);

	if (empty($flux['page'])) {
		spip_log("URL indiponible : {$url}", 'contrib');
		$reponse['erreur'] = 'url_indisponible';
	} else {
		// Transformation de la chaîne json reçue en tableau associatif
		try {
			$retour = json_decode($flux['page'], true);
			if (!empty($retour['message'])) {
				// Le traitement renvoie une erreur qui est défini dans le message
				$reponse['erreur'] = 'gitea_nok';
				$message = $retour['message'] ?? '';
				$url_message = $retour['url'] ?? $url;
				spip_log("Erreur de traitement Gitea `{$message}` pour l'url `{$url_message}`", 'contrib' . _LOG_ERREUR);
			} else {
				$reponse['donnees'] = $retour;
			}
		} catch (Exception $erreur) {
			$reponse['erreur'] = 'analyse_json';
			spip_log("Erreur d'analyse JSON pour l'URL `{$url}` : " . $erreur->getMessage(), 'contrib' . _LOG_ERREUR);
		}
	}

	return $reponse;
}

// ------ API : REQUETES GET ------

/**
 * Vérifie si un utilisateur connu par son email possède déjà un compte sur Gitea.
 *
 * @api
 *
 * @param string $email Email de l'utilisateur à vérifier
 *
 * @return bool `true` si l'utilisateur existe déjà, `false` sinon.
 */
function gitea_user_existe(string $email) : bool {
	// Par défaut on considère que le user n'existe pas
	$user_existe = false;

	if ($email) {
		$users = gitea_user_repertorier();

		// On parcours la liste pour trouver un user avec l'email fourni
		foreach ($users as $_user) {
			if ($_user['email'] === $email) {
				$user_existe = true;
				break;
			}
		}
	}

	return $user_existe;
}

/**
 * Récupère la description d'un utilisateur connu par son email.
 *
 * @api
 *
 * @param string $email Email de l'utilisateur à vérifier
 *
 * @return array Description de l'utilisateur ou tableau vide sinon.
 */
function gitea_user_lire(string $email) : array {
	// Par défaut on considère que le user n'existe pas
	$user = [];

	if ($email) {
		$users = gitea_user_repertorier();

		// On parcours la liste pour trouver un user avec l'email fourni
		foreach ($users as $_user) {
			if ($_user['email'] === $email) {
				$user = $_user;
				break;
			}
		}
	}

	return $user;
}

/**
 * Renvoie la liste des utilisateurs éventuellement filtrée.
 *
 * @api
 *
 * @param null|array $filtres Liste des filtres éventuels.
 *
 * @return array Liste des utilisateurs
 */
function gitea_user_repertorier(?array $filtres = []) : array {
	// Initialisation de l'erreur à chaine vide soit 'aucune erreur'.
	$users = [];

	include_spip('inc/ezcache_cache');
	$cache = [
		'liste' => 'all'
	];
	if ($fichier_cache = cache_est_valide('contrib', 'user', $cache)) {
		// Lecture des données du fichier cache valide et peuplement de l'index données
		$users = cache_lire('contrib', 'user', $fichier_cache);
	} else {
		// Construction de l'url de la requête de récupération de tous les users de la forge
		$parametres_path = [
			'admin',
			'users'
		];
		$url = gitea_construire_url($parametres_path);
		$reponse = gitea_requeter($url);
		if (empty($reponse['erreur'])) {
			// On stocke le tableau de tous les users.
			$users = $reponse['donnees'];

			// Création du fichier cache de tous les users
			$contenu = json_encode($users);
			cache_ecrire('contrib', 'user', $cache, $contenu);
		}
	}

	// Filtrage éventuel de la liste : on évite de parcourir la liste si pas de filtre
	if ($filtres) {
		$users_filtres = [];
		foreach ($users as $_user) {
			$user_conforme = true;
			foreach ($filtres as $_critere => $_valeur) {
				$operateur_egalite = true;
				$valeur = $_valeur;
				if (substr($_valeur, 0, 1) == '!') {
					$operateur_egalite = false;
					$valeur = ltrim($_valeur, '!');
				}
				if (
					!isset($_user[$_critere])
					or (
						isset($_user[$_critere])
						and (($operateur_egalite and ($_user[$_critere] != $valeur))
							or (!$operateur_egalite and ($_user[$_critere] == $valeur))))
				) {
					// On arrête de filtrer un user dès qu'un critère n'est pas respecté.
					$user_conforme = false;
					break;
				}
			}

			// Si le user respecte les critères on l'ajoute à la liste filtrée
			if ($user_conforme) {
				$users_filtres[] = $_user;
			}
		}
	} else {
		$users_filtres = $users;
	}

	return $users_filtres;
}

/**
 * Renvoie la liste des organisations de la forge Gitea.
 *
 * @api
 *
 * @param null|bool $inclure_repos Indique si on doit inclure les repos dans la liste ou pas (défaut false)
 *
 * @return array Liste des organisations avec ou pas les repos associés.
 */
function gitea_orga_repertorier(?bool $inclure_repos = false) : array {
	// Initialisation de l'erreur à chaine vide soit 'aucune erreur'.
	$orgas = [];

	// Requête de récupération de toutes les organisations de la forge
	$parametres_path = [
		'admin',
		'orgs'
	];
	$url = gitea_construire_url($parametres_path);
	$reponse = gitea_requeter($url);
	if (empty($reponse['erreur'])) {
		$orgas = $reponse['donnees'];
	}

	if ($inclure_repos) {
		// On rajoute pour chaque orga sa liste de repos (nom sans aucune information complémentaire)
		foreach ($orgas as $_cle => $_orga) {
			// Liste ordonnée et complète de tous les repos de l'organisation
			$repos = gitea_repo_repertorier($_orga['username']);

			// On ajoute la liste ordonnée des repos dans le tableau de l'organisation (uniquement le fullname)
			$orgas[$_cle]['repos'] = $repos
				? array_column($repos, null, 'full_name')
				: [];
		}
	}

	return $orgas;
}

/**
 * Renvoie les informations sur une team d'une organisation.
 *
 * @api
 *
 * @param string $organisation Identifiant d'une organisation
 * @param string $team_name    Identifiant d'une team de l'organisation
 *
 * @return array Tableau des informations de la team.
 */
function gitea_orga_lire_team(string $organisation, string $team_name) : array {
	// Par défaut on considère que la team n'existe pas
	$team = [];

	if (
		$organisation
		and $team_name
	) {
		// On cherche la team par son nom dans l'organisation concerné
		$parametres_path = [
			'orgs',
			$organisation,
			'teams',
			'search'
		];
		$parametres_query = [
			'q'            => $team_name,
			'include_desc' => 'false',
			'limit'        => 1
		];
		$url = gitea_construire_url($parametres_path, $parametres_query);

		// Recherche des informations sur la team
		$reponse = gitea_requeter($url);
		if (
			empty($reponse['erreur'])
			and !empty($reponse['donnees']['data'])
		) {
			// On a demandé une seule team, on la récupère.
			$team = array_shift($reponse['donnees']['data']);
		}
	}

	return $team;
}

/**
 * Renvoie la liste des repos d'une organisation donnée.
 *
 * @api
 *
 * @param string $organisation Identifiant d'une organisation
 *
 * @return array Liste des repos
 */
function gitea_repo_repertorier(string $organisation) : array {
	// Initialisation de l'erreur à chaine vide soit 'aucune erreur'.
	$repos = [];

	if ($organisation) {
		include_spip('inc/ezcache_cache');
		$cache = [
			'organisation' => $organisation,
			'contenu'      => 'full'
		];
		if ($fichier_cache = cache_est_valide('contrib', 'repo', $cache)) {
			// Lecture des données du fichier cache valide et peuplement de l'index données
			$repos = cache_lire('contrib', 'repo', $fichier_cache);
		} else {
			$parametres_path = [
				'orgs',
				$organisation,
				'repos'
			];
			$url = gitea_construire_url($parametres_path);
			$reponse = gitea_requeter($url);
			if (empty($reponse['erreur'])) {
				// Extraction des repos et classement dans l'ordre alphabétique
				$repos = $reponse['donnees'];
				$repos = array_column($repos, null, 'name');
				ksort($repos);

				// On ajoute la liste ordonnée des repos dans le tableau de l'organisation
				$repos = array_values($repos);

				// Création du cache complet des repos de l'organisation
				$contenu = json_encode($repos);
				cache_ecrire('contrib', 'repo', $cache, $contenu);
			}
		}
	}

	return $repos;
}

/**
 * Renvoie les informations sur le README.md si il existe dans la branche ou le tag du repository concerné.
 *
 * @param string      $organisation Nom de l'organisation accueillant le repository
 * @param string      $repo         Nom du repository
 * @param null|string $tag          Référence de la branche ou du tag. Si vide correspond au master
 *
 * @return array Informations sur le readme.
 */
function gitea_repo_lire_readme(string $organisation, string $repo, ?string $tag = '') : array {
	// Initialisation des informations du readme à vide.
	$readme = [
		'sha'    => '',
		'taille' => 0,
		'texte'  => '',
		'tag'    => $tag
	];

	if (
		$organisation
		and $repo
	) {
		$parametres_path = [
			'repos',
			$organisation,
			$repo,
			'contents',
			'README.md'
		];
		$parametres_query = [];
		if ($tag) {
			$parametres_query = [
				'ref' => $tag
			];
		}
		$url = gitea_construire_url($parametres_path, $parametres_query);
		$reponse = gitea_requeter($url);
		if (empty($reponse['erreur'])) {
			if (
				!empty($reponse['donnees']['content'])
				and ($texte = base64_decode($reponse['donnees']['content']))
			) {
				// Extraction et décodage du contenu du readme
				$readme['texte'] = $texte;
				$readme['sha'] = $reponse['donnees']['sha'] ?? '';
				$readme['taille'] = $reponse['donnees']['size'] ?? 0;
			}
		}
	}

	return $readme;
}

// ------ API : REQUETES POST / PUT ------

/**
 * Ajouter un utilisateur à une team.
 *
 * @api
 *
 * @param int    $id_team  Identifiant numérique de la team
 * @param string $username Nom de l"utilisateur
 *
 * @return bool `true` si l'utilisateur a été ajouté à la team, `false` sinon.
 */
function gitea_team_peupler(int $id_team, string $username) : bool {
	// Par défaut on considère que le peuplement se passe mal
	$retour = false;

	if (
		$id_team
		and $username
	) {
		// On ajoute le user connu par son nom à la team désignée par son id
		$parametres_path = [
			'teams',
			$id_team,
			'members',
			$username
		];
		$url = gitea_construire_url($parametres_path);

		// Recherche des informations sur la team
		// -- requête PUT
		$options = [
			'methode' => 'PUT'
		];
		$reponse = gitea_requeter($url, $options);
		if (empty($reponse['erreur'])) {
			// Ajout ok.
			$retour = true;
		}
	}

	return $retour;
}

/**
 * Ajouter un utilisateur à la forge.
 *
 * @api
 *
 * @param string     $username   Nom de l'utilisateur
 * @param string     $email      Email de l'utilisateur
 * @param null|array $parametres Paramètres additionnels sur l'utilisateur (par ex, la team
 *
 * @return bool `true` si l'utilisateur a été ajouté à la forge, `false` sinon.
 */
function gitea_user_creer(string $username, string $email, ?array $parametres = []) : bool {
	// Par défaut on considère que le user n'est pas créé
	$retour = false;

	if (
		$username
		and $email
	) {
		// Construction de l'url de la requête pour ajouter un suer à la forge
		$parametres_path = [
			'admin',
			'users'
		];
		$url = gitea_construire_url($parametres_path);

		// Requête, gestion des erreurs et traitement des users
		$options['datas'] = [
			'email'                => $email,
			'full_name'            => '',
			'login_name'           => $username,
			'must_change_password' => true,
			'password'             => 'BonjourGitSpip',
			'send_notify'          => true,
			'source_id'            => 0,
			'username'             => $username,
		];
		$reponse = gitea_requeter($url, $options);
		if (empty($reponse['erreur'])) {
			$retour = true;

			// On ajoute le user créé aux orgas demandées
			if (!empty($parametres['forge']['orgas'])) {
				foreach ($parametres['forge']['orgas'] as $_orga) {
					// Pour chaque orga la team s'appelle contrib.
					if (
						($team = gitea_orga_lire_team($_orga, 'contrib'))
						and $id = (int) ($team['id'])
					) {
						gitea_team_peupler($id, $username);
					}
				}
			}
		}
	}

	return $retour;
}
