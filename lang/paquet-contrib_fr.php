<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/comments.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'contrib_nom' => 'Contrib - Mécanismes et espace privé',
	'contrib_description' => 'Le plugin permet de mettre en place une structure et des mécanismes d\'échanges avec les contributeurs de façon à organiser et faciliter leurs contributions. Il propose aussi aux administrateurs une interface de contrôle pour assurer la pérennité de la structure mise en place.',
	'contrib_slogan' => 'Administrer le site SPIP-Contrib'
);
