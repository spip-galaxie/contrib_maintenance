<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/article_accueil/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'article_accueil_titre' => 'Article principal :',
	'aide' => 'Aide & à propos',

	// B
	'boite_info_explication' => 'Cette page permet aux administrateurs de SPIP-Contrib de contrôler la nouvelle organisation du site et de gérer les workflows initiés par les utilisateurs.',

	// C
	'categorie_vide_label'                      => 'Pas de catégorie',
	'configuration_page_titre'                  => 'Configuration du plugin Contrib',
	'configuration_secteur_galaxie_label'       => 'Secteurs disponibles',
	'configuration_secteur_galaxie_explication' => 'Choisir parmi les secteurs disponibles (hors secteur-carnet, secteur-apropos et secteurs-plugin) le ou les secteurs qui composeront la partie Galaxie du site.',
	'contribution_sincrire'                     => 's\'inscrire pour contribuer',
	'contribution_deja_redacteur'               => 'Bravo ! Vous êtes déjà un membre actif du site SPIP-Contrib. N\'hésitez pas à rédiger des articles et à intervenir sur les forums.',

	// D
	'dashboard_menu'         => 'Dashboard SPIP-Contrib',
	'dashboard_contrib_nom'  => 'Dashboard de SPIP-Contrib',
	'dashboard_contrib_desc' => 'Le dashboard de SPIP-Contrib permet de contrôler la cohérence de la structure définie au fil du temps. Ce dashboard utilise les objets et les mécanismes du plugin Check Factory.',

	// D
	'groupe_contrib_article_nom'  => 'Articles',
	'groupe_contrib_plugin_nom'   => 'Plugins',
	'groupe_contrib_rubrique_nom' => 'Rubriques',

	// I
	'icone_voir_plugin'                       => 'Voir le plugin',
	'icone_voir_rubrique_plugin'              => 'Voir la rubrique de documentation',
	'inscription_liste_prop_titre'            => 'Demandes d\'inscription à traiter',
	'inscription_liste_prop_sinon'            => 'Aucune demande d\'inscription à traiter',
	'inscription_liste_traite_titre'          => 'Demandes d\'inscription traitées ou refusées',
	'inscription_liste_poubelle_titre'        => 'Demandes d\'inscription clôturées',
	'inscription_page_titre'                  => 'Inscriptions',
	'inscription_site_titre'                  => 'Contribuer à la documentation',
	'inscription_developpement_titre'         => 'Contribuer au développement',
	'inscription_btn_accepter_label'          => 'Accepter',
	'inscription_btn_refuser_label'           => 'Refuser',
	'inscription_btn_cloturer_label'          => 'Clôturer',
	'inscription_form_titre'                  => 'Contribuer à SPIP',
	'inscription_fieldset_site_label'         => 'Site SPIP-Contrib',
	'inscription_fieldset_site_explication'   => 'Le site SPIP-Contrib est l\'espace privilégié de partage et de discussions des contributions à SPIP. Son espace privé est ouvert après inscription. Une fois enregistré, vous pourrez consulter les articles en cours de rédaction, proposer des articles et participer à tous les forums.',
	'inscription_forge_explication'  => 'La <a href="https://git.spip.net" class="spip_out" target="_blank">Forge SPIP</a> est l\'espace de développement de SPIP et de ses plugins. Vous pouvez <a href="https://git.spip.net/users/sign_up" class="spip_out" target="_blank">vous inscrire</a> afin de soumettre et discuter des rapports d\'anomalies ou d\'évolution ou de contribuer au code des plugins et des squelettes.',
	'inscription_fieldset_charte_label'       => 'Charte SPIP',
	'inscription_fieldset_charte_explication' => 'SPIP est un logiciel libre, chaque personne peut l’utiliser et le modifier à sa convenance. Cependant, toute participation à la communauté doit se faire dans le respect des buts et valeurs promus par le projet initial du minirézo et inscrits dans la <a href="https://www.spip.net/fr_article6431.html" class="spip_out" target="blank">Charte d\'accueil de SPIP</a>.',
	'inscription_username_label'              => 'Nom d\'utilisateur ou d\'utilisatrice',
	'inscription_email_label'                 => 'Adresse e-mail',
	'inscription_descriptif_label'            => 'Pour nous mettre l\'eau à la bouche, décrivez en quelques mots vos prochaines contributions',
	'inscription_charte_label'                => 'J\'approuve la Charte d’accueil de SPIP',
	'inscription_charte_nok_message'          => 'Pour contribuer à SPIP, vous devez approuver la charte de fonctionnement.',
	'inscription_ajout_ok_message'            => 'L\'inscription a bien été prise en compte. Un administrateur ou une administratrice va s\'en occuper rapidement et vous recevrez un email pour vous avertir de la mise à disposition de votre compte.',
	'inscription_ajout_nok_message'           => 'Erreur lors de l\'enregistrement de votre demande. Veuillez essayer ultérieurement.',
	'inscription_descriptif_nok_message'      => 'Un peu court non ? Allez, un petit effort.',
	'inscription_username_nok_message'        => 'Seuls les caractères alphanumériques et les caractères « -_ » sont autorisés.',
	'inscription_username_nok_inscex_message' => 'Une demande d\'inscription est déjà en cours avec le nom @username@.',
	'inscription_email_nok_inscex_message'    => 'Une demande d\'inscription est déjà en cours avec l\'email @email@.',
	'inscription_jamais_connecte_message'     => 'Jamais personne ne s\'y est connecté.',

	// R
	'repo_organisation_sinon'               => 'Aucun fichier disponible',
	'rubrique_plugin_generer_prefixe_label' => 'Générer les préfixes des rubriques-plugin',
	'rubrique_couleur_label'                => 'Couleur de la rubrique',
	'rubrique_forcer_texte_label'           => 'Remplacer le descriptif de la rubrique même si celle-ci en possède déjà un.',

	// T
	'type_article_label'                    => 'Type d\'article',
	'type_article_utilisation_label'        => 'documentation générale',
	'type_article_conception_label'         => 'documentation de conception',
	'type_article_actualite_label'          => 'article d\'actualité',
	'type_controle_article_prepa_nom'       => 'Articles en cours de rédaction',
	'type_controle_article_prepa_desc'      => 'Les articles sont classés par année, des plus anciens aux plus récents.',
	'type_controle_plugin_affectation_nom'  => 'Affectations avec plugin invalide',
	'type_controle_plugin_affectation_desc' => 'Ce contrôle identifie les affectations plugin-catégorie pour lesquelles le préfixe du plugin est invalide (situation provoquée par un changement de préfixe ou une disparition dudit plugin, anomalie <code>plugpfx_nok</code>).

	Pour supprimer les affectations invalides, veuillez utiliser le bouton Corriger.',
	'type_controle_plugin_categorie_nom'    => 'Plugins sans catégorie',
	'type_controle_plugin_categorie_desc'   => 'Ce contrôle permet de lister les plugins non encore affectés à une catégorie.
	Il est essentiel d\'assurer une complète correction des affectations plugin-catégorie avant d\'exécuter les contrôles liés aux rubriques-plugin.',
	'type_controle_rubrique_categorie_nom'  => 'Rubriques-catégorie en erreur',
	'type_controle_rubrique_categorie_desc' => 'Ce contrôle permet d\'identifier les catégories racine ou feuille qui ne possèdent pas de secteur ou rubrique correspondante dans le site (anomalies <code>rubcat0_abs</code>, <code>rubcat1_abs</code>) ou les celles qui en possèdent plusieurs (anomalies <code>rubcat0_max</code>, <code>rubcat1_max</code>).',
	'type_controle_rubrique_plugin_nom'     => 'Rubriques-plugin en erreur',
	'type_controle_rubrique_plugin_desc'    => 'Ce contrôle permet d\'identifier les rubriques-plugin dont le préfixe est erroné (non référencé par SVP, anomalie <code>rubplug_pfx</code>) ou celles qui sont mal positionnées dans l\'arborescence (la catégorie de la rubrique parent n\'est pas celle du plugin, anomalie <code>rubplug_loc</code>).

	Le contrôle prend pour référence la typologie &#171; catégorie des plugins &#187; qui doit donc être fiabilisée au préalable.
	Néanmoins, avant de lancer la correction automatique pour l\'anomalie <code>rubplug_loc</code>, il est conseillé de vérifier que l\'affectation plugin-catégorie est correcte. Si oui, la correction automatique placera la rubrique là où il faut.',
	'type_controle_rubrique_categorie_synchro_nom'  => 'Synchroniser les rubriques-catégorie',
	'type_controle_rubrique_categorie_synchro_desc' => 'Cette action permet de copier le titre et la description d\'une catégorie dans les champs idoines de la rubrique-catégorie associée si elle existe.',
	'type_controle_rubrique_plugin_synchro_nom'     => 'Synchroniser les rubriques-plugin',
	'type_controle_rubrique_plugin_synchro_desc'    => 'Cette action permet de copier le titre et la description d\'un plugin dans les champs idoines de la rubrique-plugin associée si elle existe.',
	'type_controle_user_recent_nom'                 => 'Utilisateurs récemment inscrits',

	// U
);
