<?php
/**
 * Ce fichier contient les fonctions de service nécessité par le plugin Check Factory.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Renvoie la configuration spécifique des caches de Gitea maintenance.
 *
 * @param string $plugin Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier ou
 *                       un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 *
 * @return array Tableau de la configuration brute du plugin.
 */
function contrib_cache_configurer(string $plugin) :array {
	// Initialisation du tableau de configuration avec les valeurs par défaut du plugin Check Factory.
	$configuration = [
		'readme' => [
			'racine'          => '_DIR_TMP',
			'sous_dossier'    => false,
			'nom_prefixe'     => 'readme',
			'nom_obligatoire' => ['prefixe'],
			'nom_facultatif'  => [],
			'extension'       => '.sha',
			'securisation'    => false,
			'serialisation'   => false,
			'decodage'        => false,
			'separateur'      => '-',
			'conservation'    => 0
		],
	];

	return $configuration;
}
