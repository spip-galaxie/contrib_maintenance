<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Ce contrôle permet d'identifier les rubriques-plugin dont le préfixe est erroné (non référencé par SVP) ou celles
 * qui sont mal positionnées dans l'arborescence (la catégorie de la rubrique parent n'est pas celle du plugin).
 *
 * @param int   $id_controle Identifiant du contrôle relatif à l'exécution de la fonction
 * @param int   $id_auteur   Identifiant de l'auteur ayant lancé le contrôle ou 0 si c'est un CRON
 * @param array $options     Paramètres nécessaires à l'exécution du contrôle
 *
 * @return string Erreur sous la forme d'une chaine (de type mot) ou vide si aucune erreur.
 */
function contrib_rubrique_plugin(int $id_controle, int $id_auteur, array $options) : string {
	// Initialisation de l'erreur à chaine vide soit 'aucune erreur'.
	$erreur = '';

	// Pour limiter le nombre de rubriques récupérées d'un coup, on boucle par secteur-plugin.
	// -- les secteurs-plugin
	include_spip('inc/contrib_rubrique');
	$secteurs_plugin = rubrique_lister_secteur_plugin();

	// Pour limiter le nombre de rubriques récupérées, on boucle par secteur.
	include_spip('inc/svp_plugin');
	include_spip('inc/contrib_plugin');
	include_spip('inc/ezcheck_observation');
	foreach ($secteurs_plugin as $_id_secteur) {
		// -- on récupère les rubriques-plugin en excluant les rubriques-plugin sans préfixe qui peuvent exister
		//    sans être une erreur.
		$filtres = [
			'id_secteur' => (int) $_id_secteur,
			'prefixe'    => '!',
			'profondeur' => 2
		];
		$informations = ['id_rubrique', 'id_parent', 'prefixe'];
		$rubriques_plugin = rubrique_repertorier($filtres, $informations);
		if ($rubriques_plugin) {
			foreach ($rubriques_plugin as $_rubrique) {
				if ($_rubrique['prefixe']) {
					// Initialisation par défaut d'une anomalie pour le type de contrôle
					$anomalie = [
						'type_controle' => 'contrib_rubrique_plugin'
					];

					// Pour chaque rubrique-plugin on vérifie que :
					// - le préfixe est bien référencé dans SVP
					// - la catégorie d'appartenance est correcte.
					if (plugin_lire($_rubrique['prefixe'], 'id_plugin')) {
						// On lit la catégorie du plugin telle que configurée dans la typologie qui est la référence:
						// - si elle n'est pas positionnée on ne renvoie aucune erreur car c'est un autre contrôle
						//   qui s'en occupe
						if ($categorie_plugin = plugin_lire_categorie($_rubrique['prefixe'])) {
							// On lit la catégorie de la rubrique parent de la rubrique en cours
							// - si elle est vide on ne renvoie pas d'erreur car c'est un autre contrôle qui s'en
							//   occupe.
							if (
								($categorie_parent = rubrique_lire($_rubrique['id_parent'], 'categorie'))
								and ($categorie_parent != $categorie_plugin)
							) {
								// La rubrique-plugin est mal positionnée dans l'arborescence alors que la rubrique
								// correspondant à la catégorie de rattachement du plugin existe.
								// - on génère une anomalie
								$anomalie['code'] = 'rubplug_loc';

								// On en profite pour stocker dans les paramètres de l'anomalie la rubrique
								// auquel elle devrait être rattachée (elle est forcément unique).
								$where = [
									'categorie=' . sql_quote($categorie_plugin)
								];
								if ($id_rubrique = sql_getfetsel('id_rubrique', 'spip_rubriques', $where)) {
									$anomalie['parametres'] = ['id_parent' => $id_rubrique];
								}
							}
						}
					} else {
						// Le préfixe du plugin n'est pas connu du référentiel SVP
						// - on génère une anomalie
						$anomalie['code'] = 'rubplug_pfx';
					}

					// Si une anomalie a été détectée on l'ajoute dans la base
					if (!empty($anomalie['code'])) {
						// On complète la description de l'anomalie et on la crée
						$anomalie['objet'] = 'rubrique';
						$anomalie['id_objet'] = $_rubrique['id_rubrique'];
						observation_ajouter(true, $id_controle, $anomalie);
					}
				}
			}
		}
	}

	return $erreur;
}

/**
 * Corrige la localisation d'une rubrique plugin (anomalie rubplug_loc).
 *
 * @param int $id_observation Identifiant de l'observation (anomalie) à corriger
 * @param int $id_auteur      Identifiant de l'auteur ayant lancé la correction automatique
 *
 * @return string Erreur sous la forme d'une chaine (de type mot) ou vide si aucune erreur.
 */
function contrib_rubrique_plugin_rubplug_loc(int $id_observation, int $id_auteur) : string {
	// Initialisation de l'erreur à chaine vide soit 'aucune erreur'.
	$erreur = '';

	// Récupération des informations nécessaires sur l'observation
	include_spip('action/editer_objet');
	$observation = objet_lire('observation', $id_observation);

	// Le champ 'parametres' de l'anomalie contient l'id du parent auquel la rubrique concernée par l'anomalie
	// devrait être rattachée.
	// - on fait la modification automatiquement.
	$parametres = unserialize($observation['parametres']);
	if (!empty($parametres['id_parent'])) {
		$maj_rubrique = [
			'id_parent' => $parametres['id_parent']
		];
		objet_modifier('rubrique', (int) ($observation['id_objet']), $maj_rubrique);
	} else {
		$erreur = 'corr_nok';
	}

	return $erreur;
}

/**
 * Ce contrôle permet d'identifier les catégories racine ou feuille qui ne possèdent pas de rubrique
 * correspondante dans le site.
 *
 * @param int   $id_controle Identifiant du contrôle relatif à l'exécution de la fonction
 * @param int   $id_auteur   Identifiant de l'auteur ayant lancé le contrôle ou 0 si c'est un CRON
 * @param array $options     Paramètres nécessaires à l'exécution du contrôle
 *
 * @return string Erreur sous la forme d'une chaine (de type mot) ou vide si aucune erreur.
 */
function contrib_rubrique_categorie(int $id_controle, int $id_auteur, array $options) : string {
	// Initialisation de l'erreur à chaine vide soit 'aucune erreur'.
	$erreur = '';

	// Récupération de tous les types de plugin de la typologie 'categorie' avec leur identifiant et leur
	// profondeur
	$typologie = 'categorie';
	include_spip('inc/svptype_type_plugin');
	$categories = type_plugin_repertorier(
		$typologie,
		[],
		['identifiant', 'profondeur', 'id_mot']
	);

	// On boucle sur chacune des catégories en recherchant dans les rubriques si il en existe une dont le champ
	// 'categorie' correspond au champ 'identifiant' du type de plugin.
	include_spip('inc/contrib_rubrique');
	include_spip('inc/ezcheck_observation');
	foreach ($categories as $_categorie) {
		// Initialisation par défaut d'une anomalie pour le type de contrôle
		$anomalie = [
			'type_controle' => 'contrib_rubrique_categorie'
		];

		// On cherche si il existe une ou plusieurs rubriques associées à la catégorie. Le retour est toujours un
		// tableau.
		$rubriques = rubrique_repertorier(
			['categorie' => $_categorie['identifiant']],
			['id_rubrique']
		);
		$rubriques = array_column($rubriques, 'id_rubrique');

		if (count($rubriques) != 1) {
			// C'est forcément une erreur. On détermine le préfixe de l'erreur en fonction de la profondeur
			// du type de plugin (0 ou 1)
			$prefixe_anomalie = "rubcat{$_categorie['profondeur']}";

			// Suivant que la rubrique est introuvable ou que plusieurs rubriques ont la même catégorie on complète
			// le code de l'anomalie
			$anomalie['code'] = $prefixe_anomalie
				. '_'
				. (!$rubriques ? 'abs' : 'max');

			// On complète la description de l'anomalie avec l'objet et les paramètres
			// et on la crée.
			$anomalie['objet'] = $_categorie['identifiant'];
			$anomalie['url_objet'] = parametre_url(
				parametre_url(
					generer_url_ecrire('type_plugin'),
					'typologie',
					$typologie
				),
				'id_mot',
				$_categorie['id_mot']
			);
			if ($rubriques) {
				$anomalie['parametres'] = ['rubriques' => implode(', ', $rubriques)];
			}
			observation_ajouter(true, $id_controle, $anomalie);
		}
	}

	return $erreur;
}

/**
 * Ce contrôle permet de lancer la synchronisation des titres et descriptions des rubriques-plugin.
 *
 * @param int   $id_controle Identifiant du contrôle relatif à l'exécution de la fonction
 * @param int   $id_auteur   Identifiant de l'auteur ayant lancé le contrôle ou 0 si c'est un CRON
 * @param array $options     Paramètres nécessaires à l'exécution du contrôle
 *
 * @return string Erreur sous la forme d'une chaine (de type mot) ou vide si aucune erreur.
 */
function contrib_rubrique_plugin_synchro(int $id_controle, int $id_auteur, array $options) : string {
	// Initialisation de l'erreur à chaine vide soit 'aucune erreur'.
	$erreur = '';

	// On appelle l'action associée avec l'indicateur de forcage ou pas suivant la demande
	$synchroniser = charger_fonction('rubrique_plugin_synchroniser_texte', 'action');
	$synchroniser($options['forcer_texte']);

	return $erreur;
}

/**
 * Ce contrôle permet de lancer la synchronisation des titres et descriptions des rubriques-catégorie.
 *
 * @param int   $id_controle Identifiant du contrôle relatif à l'exécution de la fonction
 * @param int   $id_auteur   Identifiant de l'auteur ayant lancé le contrôle ou 0 si c'est un CRON
 * @param array $options     Paramètres nécessaires à l'exécution du contrôle
 *
 * @return string Erreur sous la forme d'une chaine (de type mot) ou vide si aucune erreur.
 */
function contrib_rubrique_categorie_synchro(int $id_controle, int $id_auteur, array $options) : string {
	// Initialisation de l'erreur à chaine vide soit 'aucune erreur'.
	$erreur = '';

	// On appelle l'action associée avec l'indicateur de forcage ou pas suivant la demande
	$synchroniser = charger_fonction('rubrique_categorie_synchroniser_texte', 'action');
	$synchroniser($options['forcer_texte']);

	return $erreur;
}

/**
 * Ce contrôle permet d'identifier les affectations plugin-catégorie pour lesquelles le préfixe de plugin
 * est erroné (c'est le cas si celui-ci a été changé ou si le plugin a été retiré du référentiel).
 *
 * @param int   $id_controle Identifiant du contrôle relatif à l'exécution de la fonction
 * @param int   $id_auteur   Identifiant de l'auteur ayant lancé le contrôle ou 0 si c'est un CRON
 * @param array $options     Paramètres nécessaires à l'exécution du contrôle
 *
 * @return string Erreur sous la forme d'une chaine (de type mot) ou vide si aucune erreur.
*/
function contrib_plugin_affectation(int $id_controle, int $id_auteur, array $options) : string {
	// Initialisation de l'erreur à chaine vide soit 'aucune erreur'.
	$erreur = '';

	// Récupération des plugins du référentiel sous la forme [prefixe] = id_plugin
	$select = ['prefixe', 'spip_plugins.id_plugin as id_plugin'];
	$from = ['spip_plugins', 'spip_depots_plugins'];
	$group_by = ['spip_plugins.id_plugin'];
	$where = ['spip_depots_plugins.id_depot>0', 'spip_depots_plugins.id_plugin=spip_plugins.id_plugin'];
	$plugins = sql_allfetsel($select, $from, $where, $group_by);
	$plugins = array_column($plugins, 'id_plugin', 'prefixe');

	// Récupération des affectations plugin-catégorie sous la forme [prefixe] = affectation
	include_spip('inc/svptype_type_plugin');
	$affectations = type_plugin_repertorier_affectation('categorie');
	$affectations = array_column($affectations, null, 'prefixe');

	if ($affectations) {
		$affectations_erronees = array_diff_key($affectations, $plugins);
		if ($affectations_erronees) {
			include_spip('inc/ezcheck_observation');
			// On ajoute une anomalie pour chaque affectation erronée
			foreach ($affectations_erronees as $_prefixe => $_affectation) {
				// Initialisation de l'anomalie plugaff_pfx.
				$anomalie = [
					'type_controle' => 'contrib_plugin_affectation',
					'code'          => 'plugpfx_nok',
					'parametres'    => $_affectation,
					'objet'         => "{$_prefixe}-{$_affectation['identifiant_mot']}"
				];

				// Création de l'anomalie
				observation_ajouter(true, $id_controle, $anomalie);
			}
		}
	}

	return $erreur;
}

/**
 * Corrige une affectation de catégorie à un plugin dont le préfixe est erronné en la supprimant.
 *
 * @param int $id_observation Identifiant de l'observation (anomalie) à corriger
 * @param int $id_auteur      Identifiant de l'auteur ayant lancé la correction automatique
 *
 * @return string Erreur sous la forme d'une chaine (de type mot) ou vide si aucune erreur.
 */
function contrib_plugin_affectation_plugpfx_nok(int $id_observation, int $id_auteur) : string {
	// Initialisation de l'erreur à chaine vide soit 'aucune erreur'.
	$erreur = '';

	// Récupération des informations nécessaires sur l'observation
	include_spip('action/editer_objet');
	$observation = objet_lire('observation', $id_observation);

	// Le champ 'parametres' de l'anomalie contient toutes les champs qui composent l'affectation (table de liens).
	// - on supprime le lien à partir de sa clé composée de l'id du groupe, l'id du mot et le préfixe du plugin.
	$parametres = unserialize($observation['parametres']);
	if (
		!empty($parametres['id_groupe'])
		and !empty($parametres['id_mot'])
		and !empty($parametres['prefixe'])
	) {
		$where = [
			'id_groupe=' . (int) ($parametres['id_groupe']),
			'id_mot=' . (int) ($parametres['id_mot']),
			'prefixe=' . sql_quote($parametres['prefixe'])
		];
		sql_delete('spip_plugins_typologies', $where);
	} else {
		$erreur = 'corr_nok';
	}

	return $erreur;
}
